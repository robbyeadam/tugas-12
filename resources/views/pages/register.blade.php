@extends('layout.master')
@section('title')
    Registration
    
@endsection

@section('isi')
<h1>Halaman Pendaftaran </h1>
<form action="{{url('/welcome')}}" method="post">

    @csrf
    
    <label>Nama Depan :</label> <br>
    <input type="text" name="ND"><br><br>
    <label>Nama Belakang :</label><br>
    <input type="text" name="NB"><br><br>
    
    <label> Jenis Kelamin</label><br>
    <input type="radio"  name="JK"> Laki - Laki<br>
    <input type="radio" name="JK"> Perempuan <br><br>


    <label>Kewarganegaraan</label><br>
    <select name="Kewarganegaraan">
        <option value="1">Indonesia</option> 
        <option value="2">Asing</option> 
    </select><br><br>

    <label>Bahasa</label> <br>
    <input type="checkbox"> Bahasa Indonesia <br>
    <input type="checkbox"> English<br>
    <input type="checkbox"> Lainnya / Other <br><br>

    <label>Bio</label><br>
    <textarea name="Bio" rows="6" cols="25"></textarea><br>

    <input type="submit" class="register" value="Sign In"> 
    
@endsection
