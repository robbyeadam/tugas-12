@extends('layout.master')
@section('title')
    Home Page
@endsection

@section('isi')
<h1> Sanberbook</h1>
<h2> Social Media Developer</h2>
<p> Belajar dan Berbagi agar hidup ini semakin santai dan berkualitas</p>
<h3>Benefit bergabung di Sanberbook</h3>

<ul>
<li>Mendapat motivasi dari sesama developer</li>
<li>Mendapat pengetahuan dari expert</li>
<li>Menjadi calon web developer yang terbaik</li> 
</ul>

<h3>Cara bergabung ke Sanberbook </h3>
<ol>
<li>Kunjungi website Sanberbook  </li>
<li>Registrasi di form ini <a href="/register"> Form signup</a> </li>
<li>Selesai ! </li>
</ol>
    
@endsection
