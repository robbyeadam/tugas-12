<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@home');

Route::get('/register', 'App\Http\Controllers\AuthController@register');

Route::post('/welcome', 'App\Http\Controllers\AuthController@welcome');

Route::get('/table', function(){
    return view('index-table.table');
});

Route::get('/data-table', function(){
    return view('index-table.data-table');
});

Route::get('/cast/create','App\Http\Controllers\CastController@create');
Route::post('/cast','App\Http\Controllers\CastController@store');

Route::get('/cast','App\Http\Controllers\CastController@index');
Route::get('/cast/{cast_id}','App\Http\Controllers\CastController@show');

Route::get('/cast/{cast_id}/edit','App\Http\Controllers\CastController@edit');
Route::put('/cast/{cast_id}','App\Http\Controllers\CastController@update');

Route::delete('/cast/{cast_id}','App\Http\Controllers\CastController@destroy');